package com.egypteam.ai.clever.tcim.testcases;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CarrinhoComeAZerado {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testCarrinhoComeAZerado() throws Exception {
    driver.get("http://deposito.demo.egypteam.com/ui");
    driver.findElement(By.name("email")).click();
    driver.findElement(By.name("email")).clear();
    driver.findElement(By.name("email")).sendKeys("ti@egypteam.com");
    driver.findElement(By.name("password")).clear();
    driver.findElement(By.name("password")).sendKeys("P@ssw0rd.Welcome@123");
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    driver.findElement(By.cssSelector("a.dropdown-toggle.count-info > svg.svg-inline--fa.fa-shopping-cart.fa-w-18 > path")).click();
    String TEST = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Total:'])[2]/following::span[1]")).getText();
    System.out.println(TEST);
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='R$ 444,00'])[1]/following::div[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='See All Alerts'])[1]/following::a[1]")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [getExpression | ${TEST} | ]]
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
