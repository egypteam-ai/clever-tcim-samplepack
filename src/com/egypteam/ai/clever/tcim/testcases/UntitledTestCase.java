package com.egypteam.ai.clever.tcim.testcases;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import com.egypteam.ai.clever.tcim.CleverTcimTestCase;

public class UntitledTestCase extends CleverTcimTestCase {
  @Test
  public boolean test() throws Exception
  {  
	  
    driver.get("http://deposito.demo.egypteam.com/ui/login?resource=%2Fui%2Fhome");
    driver.findElement(By.name("email")).click();
    driver.findElement(By.name("email")).clear();
    driver.findElement(By.name("email")).sendKeys("ti@egypteam.com");
    driver.findElement(By.name("password")).clear();
    driver.findElement(By.name("password")).sendKeys("P@ssw0rd.Welcome@123");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Tudo em um só lugar.'])[1]/following::button[1]")).click();
    driver.findElement(By.cssSelector("a.dropdown-toggle.count-info > svg.svg-inline--fa.fa-shopping-cart.fa-w-18 > path")).click();
    String VLR_TOTAL_CARRINHO = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Total:'])[2]/following::span[1]")).getText();

    //driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='R$ 444,00'])[1]/following::div[1]")).click();
    //driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='See All Alerts'])[1]/following::a[1]")).click();

    if (VLR_TOTAL_CARRINHO.equals("R$ 0.00")) {
    	return true;
    }
    return false;

  }
}
